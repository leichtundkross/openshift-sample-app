## About
Sample project to demonstrate usage of OpenShift's source 2 image

## How to use
Just import the image stream into your OpenShift environment

         oc create -f https://gitlab.com/leichtundkross/openshift-sample-app/raw/master/s2i-java-imagestream.json